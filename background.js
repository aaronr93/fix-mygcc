// Author: Aaron Rosenberger

// Update the declarative rules on install or upgrade.
chrome.runtime.onInstalled.addListener(function() {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    chrome.declarativeContent.onPageChanged.addRules([{
      conditions: [
        // When a page is on my.gcc.edu
        new chrome.declarativeContent.PageStateMatcher({
		  pageUrl: { urlContains: 'my.gcc.edu' },
        })
      ],
      // ... show the page action.
      actions: [new chrome.declarativeContent.ShowPageAction() ]
    }]);
  });
});