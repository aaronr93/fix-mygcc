(window.onload = function() {
	function Fix() {
		this.html_items = {
			myCourses: document.getElementById('myCourses'),
			quickLinks: document.getElementById('quickLinks'),
			masthead: document.getElementById('masthead'),
			targetedMessage: document.getElementById('TargetedMessage'),
			admissionsApplicationLink: document.getElementById('globalLinks').firstChild,
			parentPortalInstructionsLink: document.getElementById('globalLinks').lastChild,
			myPages: document.getElementById('myPages'),
			sideSection: document.getElementById('sideBar'),
			headerTabs: {
				Home: document.getElementsByClassName('tab_Home')[0],
				Campus_Life: document.getElementsByClassName('tab_Campus_Life')[0],
				Student: document.getElementsByClassName('tab_Student')[0],
				Financial_Info: document.getElementsByClassName('tab_Financial_Info')[0],
				Sign_Up: document.getElementsByClassName('tab_Sign_Up')[0],
				Academics: document.getElementsByClassName('tab_Academics')[0],
				Events: document.getElementsByClassName('tab_Events')[0],
				Career_Services: document.getElementsByClassName('tab_Career_Services')[0],
				Timecard_Entry: document.getElementsByClassName('tab_Timecard_Entry')[0],
				Seniors: document.getElementsByClassName('tab_Seniors')[0],
				My_Pages: document.getElementsByClassName('tab_My_Pages')[0]
			},
			campusLifeUselessImage: document.getElementsByClassName('pt_CustomContentPortlet')[0],
			courseworkItems: document.getElementsByClassName('assignmentDisplay')
		};
		this.settings = {
			keepSessionAlive: true,
			removeAll: true,
			autoExpandClasses: true,
			filterHeaderTabs: {
				signUp: this.html_items.headerTabs.Sign_Up,
				events: this.html_items.headerTabs.Events,
				myPages: this.html_items.headerTabs.My_Pages
			},
			collapseCampusLifeUselessImage: true,
			removeSideSectionOn: [
				'Home',
				'Campus Life',
				'Financial Info',
				'Sign Up',
				'Academics',
				'Events',
				'Career Services',
				'Timecard Entry',
				'Seniors',
				'My Pages'
			]
		};
	}
	
	Fix.prototype.try_keepSessionAlive = function () {
		if (!this.settings.keepSessionAlive) return;
		try {
			keepSessionAlive();
		} catch (error) {
			logThis(error);
		}
	};
	
	Fix.prototype.try_removeAll = function () {
		if (!this.settings.removeAll) return;
		try {
			removeAll(this.html_items);
		} catch (error) {
			logThis(error);
		}
	};
	
	Fix.prototype.try_autoExpandClasses = function () {
		if (!this.settings.autoExpandClasses) return;
		try {
			autoExpandClasses(this.html_items);
		} catch (error) {
			logThis(error);
		}
	};
	
	Fix.prototype.try_filterHeaderTabs = function () {
		try {
			filterHeaderTabs(this.settings.filterHeaderTabs);
		} catch (error) {
			logThis(error);
		}
	};
	
	Fix.prototype.try_removeSideSectionOn = function () {
		if (!this.settings.removeSideSectionOn) return;
		try {
			removeSideSectionOn(this.settings.removeSideSectionOn);
		} catch (error) {
			logThis(error);
		}
	};
	
	Fix.prototype.try_showGradesOnHover = function () {
		if (!this.settings.showGradesOnHover) return;
		try {
			showGradesOnHover(this.html_items.courseworkItems);
		} catch (error) {
			logThis(error);
		}
	};
	
	Fix.prototype.determineMethodsToCall = function () {
		this.try_keepSessionAlive();
		this.try_removeAll();	
		this.try_autoExpandClasses();
		this.try_filterHeaderTabs();
		this.try_removeSideSectionOn();
		this.try_collapseCampusLifeUselessImage();
		this.try_showGradesOnHover();
	};

	var fix = new Fix();
	fix.determineMethodsToCall();
	
})();
		
function keepSessionAlive() {
	setInterval(tryToKeepTabAlive, 30000);
}

function removeSideSectionOn(pages) {
	var currentSideSection = this.html_items.sideSection.firstChild.firstChild;
	for (var i = 0; i < pages.length; i++) {
		if (currentSideSection.title === pages[i]) {
			this.html_items.sideSection.remove();
			return;
		}
	}
}

function removeAll(html_items) {
	html_items.masthead.remove();
	html_items.targetedMessage.remove();
	html_items.admissionsApplicationLink.remove();
	html_items.parentPortalInstructionsLink.remove();
	html_items.myPages.remove();
	html_items.campusLifeUselessImage.remove();
}

function autoExpandClasses(html_items) {
	html_items.quickLinks.appendChild(html_items.myCourses.childNodes[1]);
	html_items.myCourses.remove();
	html_items.quickLinks.firstChild.innerHTML = 'My Courses';
	html_items.quickLinks.style.paddingBottom = '7px';
}

function filterHeaderTabs(unwantedTabs) {
	for (var key in unwantedTabs) {
		if (!key) throw "Error in filterHeaderTabs: Key does not exist";
		unwantedTabs[key].remove();
	}
}

function tryToKeepTabAlive() {
	try {
		keepTabAlive();
	} catch (error) {
		logThis(error);
	}
}

function keepTabAlive() {
	try {
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "https://my.gcc.edu/ICS/services/sessionkeepalive.asmx/Ping", true);
		xhr.onreadystatechange = notifyStatusReady;
		xhr.send();	
	} catch (error) {
		throw error;
	}
}

function notifyStatusReady(readystatechange) {
	var xmlhttp = readystatechange.currentTarget;

	if (xmlhttp.status === 404) {
		throw "404 not found";
	}
}

function showGradesOnHover(courseworkItems) {
	var grade;
	for (var i = 0; i < courseworkItems.length; i++) {
		grade = courseworkItems[i].firstChild.lastChild;
		hideThis(grade);
		courseworkItems[i].blur();
		courseworkItems[i].onfocus = showThis(grade);
	}
}

function hideThis(section) {
	if (section) {
		section.style.display = "none";
	} else {
		throw "Section to hide is not defined.";
	}
}

function showThis(section) {
	if (section) {
		section.style.display = "block";
	} else {
		throw "Section to show is not defined.";
	}
}

function logThis(msg) {
	console.log(msg);
}